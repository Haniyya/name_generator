require 'sqlite3'
require 'sequel'

require_relative 'lib/utility'
require_relative 'lib/database'

module Name
  class Generator
    attr_reader :adjectives, :nouns

    def initialize
      @adjectives = DB[:adjectives]
      @nouns      = DB[:nouns]
    end

    def random_noun
      get_random(:nouns)
    end

    def random_adjective
      get_random(:adjectives)
    end

    private

    def get_random(table)
      id = rand(DB[table].count)
      DB[table].first(id: id)
    end
  end
end
