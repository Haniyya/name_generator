module Name
  class Utility
    class << self
      def data_path(path)
        File.join(base_folder_path, 'data', path)
      end

      def tmp_path(path)
        File.join(base_folder_path, 'tmp', path)
      end

      def base_folder_path(path = '')
        File.join(File.dirname(__FILE__), '../', path)
      end
    end
  end
end
