module Name
  DB = Sequel.sqlite(Utility.tmp_path('words.db'))

  unless DB.table_exists?(:adjectives)
    DB.run File.read(Utility.data_path('db_init.sql'))
  end

  DB.create_table? :adjectives do
    primary_key :id
    String :value, index: true
  end

  DB.create_table? :nouns do
    primary_key :id
    String :value, index: true
  end
end
