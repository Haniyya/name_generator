require 'grape'

require_relative 'application'

class NameAPI < Grape::API
  format :json
  prefix :api

  before do
    header "Access-Control-Allow-Origin", "*"
  end

  helpers do
    def generator
      @generator ||= Name::Generator.new
    end
  end

  get '/adjective' do
    {adjective: generator.random_adjective}
  end

  get '/noun' do
    {noun: generator.random_noun}
  end

  get '/full' do
    {
      adjective: generator.random_adjective,
      noun: generator.random_noun
    }
  end
end
